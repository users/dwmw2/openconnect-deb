
DISTREV := 1
MODULEDIR := .git/modules/openconnect
CHANGELOG_DEPS = $(wildcard $(MODULEDIR)/index $(MODULEDIR)/packed-refs $(MODULEDIR)/refs/tags $(MODULEDIR)/HEAD)
OCVERSION := $(shell cd openconnect && git describe --tags | sed 's/-g[0-9a-f]\*//')
PKGVERSION := $(shell echo $(OCVERSION) | sed  -e 's/$$/-0/' -e 's/v\([^-]\+-[^-]\+\)\(-g[0-9a-f-]\+\|\)/\1/')
OCDATE += $(shell cd openconnect && git show -s --pretty=%aD)

ppa: bionic focal

debian/libopenconnect5.symbols: gensyms.sh openconnect/openconnect.h openconnect/libopenconnect.map.in
	./gensyms.sh > $@ || rm $@

cleandir: debian/libopenconnect5.symbols
	cd openconnect; \
	git reset --hard HEAD; \
	git clean -fdx; \
	NOCONFIGURE=1 ./autogen.sh; \
	libtoolize --automake --force --copy; \
	automake --foreign --add-missing --force --copy
	cp -av debian openconnect

xenial bionic focal: cleandir
	sed -e "s/PKGVER/$(PKGVERSION)/" -e "s/GITVER/$(OCVERSION)/" \
	    -e "s/COMMITDATE/$(OCDATE)/" -e "s/DISTRO/$@/g" \
	    -e "s/DISTREV/$(DISTREV)/" debian/changelog.in > openconnect/debian/changelog
	cd openconnect && DEBUILD_TGZ_CHECK=no debuild -S
	dput openconnect openconnect_$(PKGVERSION)-$@$(DISTREV)_source.changes

bionic-binary xenial-binary focal-binary: cleandir
	sed -e "s/PKGVER/$(PKGVERSION)/" -e "s/GITVER/$(OCVERSION)/" \
	    -e "s/COMMITDATE/$(OCDATE)/" -e "s/DISTRO/$(patsubst %-binary,%,$@)/g" \
	    -e "s/DISTREV/$(DISTREV)/" debian/changelog.in > openconnect/debian/changelog
	cd openconnect && DEBUILD_TGZ_CHECK=no debuild -b

