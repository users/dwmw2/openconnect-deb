#!/bin/bash

set -e
THISVER=$(cd openconnect && git describe --tags | sed "s/v\([0-9.]\+\(-[0-9]\+\)\?\).*/\1/")

function extractsyms() {
    SYMVER="$1"
    PKGVER="$2"

    echo " $SYMVER@$SYMVER $PKGVER"
    sed -n -e "/^$SYMVER/,/^}/{/openconnect_/s/^\t\(openconnect_.*\);/ \1@$SYMVER $PKGVER/p}" openconnect/libopenconnect.map.in
}

echo "libopenconnect.so.5 libopenconnect5 #MINVER#"
( extractsyms OPENCONNECT_PRIVATE $THISVER; 
  sed -n openconnect/openconnect.h -e "/^ \* API version/{\
    s/.* API version \([0-9.]\+\) (v\([0-9.]\+\);.*/\1 \2/;\
    s/.* API version \([0-9.]\+\):.*/\1 $THISVER/;p}" |
     while read APIVER PKGVER; do
	 SYMVER=${APIVER/./_}
	 case $APIVER in
	     5.0)
		 # Yes, "5.0" not "5_0". And I've really only just noticed but
		 # now it's ABI, and has been for years.
		 SYMVER="OPENCONNECT_5.0"
		 ;;

	     5.*)
		 SYMVER=OPENCONNECT_${APIVER/./_}
		 ;;

	     *)
		 continue
		 ;;
	 esac
	 extractsyms "$SYMVER" "$PKGVER"
     done ) | LANG=C sort

